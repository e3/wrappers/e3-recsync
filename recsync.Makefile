where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile


APP:=client/castApp
APPDB:=$(APP)/Db
APPSRC:=$(APP)/src

USR_CPPFLAGS += -DUSE_TYPED_RSET
USR_INCLUDES += -I$(where_am_I)$(APPSRC)

SOURCES += $(APPSRC)/sockhelpers.c
SOURCES += $(APPSRC)/caster.c
SOURCES += $(APPSRC)/castudp.c
SOURCES += $(APPSRC)/casttcp.c
SOURCES += $(APPSRC)/castinit.c
SOURCES += $(APPSRC)/dbcb.c

TEMPLATES += $(APPDB)/reccaster.db

DBDS += $(APPSRC)/reccaster.dbd

SCRIPTS += ../iocsh/recsync.iocsh

.PHONY: vlibs
vlibs:
