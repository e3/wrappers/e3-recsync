# e3-recsync

Wrapper for the `recSync` module - part of EPICS infrastructure at ESS.

This module provides the client (RecCaster) which runs as part of an EPICS IOC. It sends the PV list in the IOC to a receiving server to populate the ChannelFinder directory service.

## Installation

```sh
$ make init patch build
$ make install
```

For further targets, type `make`.

## Usage

```sh
$ iocsh -r "recsync"
```

## Contributing

Contributions through pull/merge requests only.

